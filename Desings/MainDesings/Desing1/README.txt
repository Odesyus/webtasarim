A Pen created at CodePen.io. You can find this one at https://codepen.io/ryankbrown/pen/eWNRGr.

 I'm creating the illusion of pressure on a dot grid using purely CSS.  (JS is only used to rotate the grid for the viewer and for the custom mouse cursor). This was created by using the :active pseudo class on an invisible, absolutely positioned "touch target" and using a ton of sibling selectors.